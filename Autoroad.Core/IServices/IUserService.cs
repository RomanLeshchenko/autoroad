﻿using Autoroad.Data.Models;


namespace Autoroad.Core.IServices
{
    public interface IUserService
    {
        User GetUserPersonalData(int userId);
        User EditUserPersonalData(User model);
    }
}
