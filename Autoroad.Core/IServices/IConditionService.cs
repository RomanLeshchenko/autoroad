﻿using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.IServices
{
    public interface IConditionService
    {
        IEnumerable<Condition> GetConditions();
    }
}
