﻿using Autoroad.Core.ViewModels;
using Autoroad.Data.Models;
using Autoroad.Data.ViewModels;
using System.Collections.Generic;

namespace Autoroad.Core.IServices
{
    public interface IAdvertService
    {
        IEnumerable<AdvertViewModel> GetSearchAdvertResult(SearchAdvertViewModel model);
        IList<UserAdvertViewModel> GetUserAdverts(int userId);
        IEnumerable<AdvertViewModel> GetAdvertsByBrand(int brandId);
        IEnumerable<AdvertViewModel> GetAdvertsByRegion(int regionId);
        SearchAdvertViewModel SearchAdvert();
        Advert AddAdvert(int userId, Advert model);
        AdvertViewModel GetAdvert(int advertId, int? userWhoLookedId);
        Advert EditAdvert(int userId, Advert model);
        Advert UpdateAdvertDateAdded(int userId, int advertId);
        void RemoveAdvert(int userId, int advertId);
        int? AddAdvertToFavourite(int userId, int advertId);
        IEnumerable<FavouriteAdvert> GetFavouriteAdverts(int userId);
        void RemoveAdvertFromFavourites(int userId, int advertId);
    }
}
