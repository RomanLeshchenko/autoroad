﻿using Autoroad.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Autoroad.Core.IServices
{
    public interface IPhotoService
    {
        string GetNewFileName(HttpPostedFile file);
        void SavePNGFile(HttpPostedFile file, string fname, string newFileName, int advertId);
        void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath, string newFileName, int advertId);
        IEnumerable<ExistingPhotoViewModel> GetExistingPhotos();
    }
}
