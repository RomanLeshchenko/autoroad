﻿using Autoroad.Data.Models;
using System.Collections.Generic;


namespace Autoroad.Core.IServices
{
    public interface IEngineService
    {
        IEnumerable<Engine> GetEngines();
    }
}
