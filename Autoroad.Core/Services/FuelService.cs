﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
    public class FuelService : IFuelService
    {
        private readonly IFuelRepository _fuelRepository;

        public FuelService(IFuelRepository fuelRepository)
        {
            _fuelRepository = fuelRepository;
        }

        public IEnumerable<Fuel> GetFuels()
        {
            return _fuelRepository.GetAll();
        }
    }
}
