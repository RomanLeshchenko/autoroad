﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
	public class DriveUnitService : IDriveUnitService
	{
		private readonly IDriveUnitRepository _driveUnitRepository;

		public DriveUnitService(IDriveUnitRepository driveUnitRepository)
		{
			_driveUnitRepository = driveUnitRepository;
		}

		public IEnumerable<DriveUnit> GetDriveUnits()
		{
			return _driveUnitRepository.GetAll();
		}
	}
}