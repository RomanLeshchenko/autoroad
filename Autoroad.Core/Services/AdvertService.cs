﻿using AutoMapper;
using Autoroad.Core.IServices;
using Autoroad.Core.ViewModels;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using Autoroad.Data.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Autoroad.Core.Services
{
    public class AdvertService : IAdvertService
    {
        private readonly IUserRepository _userRepository;
        private readonly IReleaseYearRepository _releaseYearRepository;
        private readonly IAdvertRepository _advertReposity;
        private readonly IBrandRepository _brandRepository;
        private readonly IRegionRepository _regionRepository;
        private readonly IModelRepository _modelRepository;
        private readonly IEngineRepository _engineRepository;
        private readonly IFuelRepository _fuelRepository;
        private readonly ITransmissionRepository _transmissionRepository;
        private readonly IBodyTypeRepository _bodyTypeRepository;
        private readonly IRegistrationRepository _registrationRepository;
        private readonly IConditionRepository _conditionRepository;
        private readonly IAdvertCountViewRepository _advertCountViewRepository;
        private readonly IMapper _mapper;
        private readonly IFavouriteAdvertRepository _favouriteAdvertRepository;
        private readonly IExchangeService _exchangeService;
        private readonly IPossibleExchangeCarRepository _possibleExchangeRepository;
        private readonly IAdvertPhotoRepository _advertPhotoRepository;
        private readonly IPhotoInAdvertRepository _photoInAdvertRepository;

        public AdvertService(IPhotoInAdvertRepository photoInAdvertRepository, IAdvertPhotoRepository advertPhotoRepository, IPossibleExchangeCarRepository possibleExchangeRepository, IFavouriteAdvertRepository favouriteAdvertRepository, IUserRepository userRepository, IAdvertRepository advertRepository, IBrandRepository brandRepository, IRegionRepository regionRepository, IReleaseYearRepository releaseYearRepository, IModelRepository modelRepository, IEngineRepository engineRepository, IFuelRepository fuelRepository, ITransmissionRepository transmissionRepository, IBodyTypeRepository bodyTypeRepository, IRegistrationRepository registrationRepository, IConditionRepository conditionRepository, IAdvertCountViewRepository advertCountViewRepository, IMapper mapper, IExchangeService exchangeService)
        {
            _photoInAdvertRepository = photoInAdvertRepository;
            _advertPhotoRepository = advertPhotoRepository;
            _possibleExchangeRepository = possibleExchangeRepository;
            _advertCountViewRepository = advertCountViewRepository;
            _releaseYearRepository = releaseYearRepository;
            _modelRepository = modelRepository;
            _userRepository = userRepository;
            _advertReposity = advertRepository;
            _brandRepository = brandRepository;
            _regionRepository = regionRepository;
            _engineRepository = engineRepository;
            _fuelRepository = fuelRepository;
            _transmissionRepository = transmissionRepository;
            _registrationRepository = registrationRepository;
            _conditionRepository = conditionRepository;
            _bodyTypeRepository = bodyTypeRepository;
            _favouriteAdvertRepository = favouriteAdvertRepository;
            _exchangeService = exchangeService;
            _mapper = mapper;
        }

        public Advert AddAdvert(int userId, Advert model)
        {
            if (model != null)
            {
                var user = _userRepository.Get(x => x.UserId == userId);

                if (user != null)
                {
                    if (model.PossibleExchangeCar.EngineId.HasValue && model.PossibleExchangeCar.EngineTwoId.HasValue)
                    {
                        var engineFrom = _engineRepository.Get(x => x.EngineId == model.PossibleExchangeCar.EngineId);
                        var engineTo = _engineRepository.Get(x => x.EngineId == model.PossibleExchangeCar.EngineTwoId);

                        if (engineFrom.Name > engineTo.Name)
                        {
                            model.PossibleExchangeCar.EngineId = engineTo.EngineId;
                            model.PossibleExchangeCar.EngineTwoId = engineFrom.EngineId;
                        }
                    }

                    if (model.PossibleExchangeCar.YearStartId.HasValue && model.PossibleExchangeCar.YearEndId.HasValue)
                    {
                        var yearStart = _releaseYearRepository.Get(x => x.ReleaseYearId == model.PossibleExchangeCar.YearStartId);
                        var yearEnd = _releaseYearRepository.Get(x => x.ReleaseYearId == model.PossibleExchangeCar.YearEndId);

                        if (yearStart.Year > yearEnd.Year)
                        {
                            model.PossibleExchangeCar.YearStartId = yearEnd.ReleaseYearId;
                            model.PossibleExchangeCar.YearEndId = yearStart.ReleaseYearId;
                        }
                    }

                    if (model.PossibleExchangeCar.PriceStart > model.PossibleExchangeCar.PriceEnd)
                    {
                        var priceStart = model.PossibleExchangeCar.PriceStart;
                        var priceEnd = model.PossibleExchangeCar.PriceEnd;

                        model.PossibleExchangeCar.PriceStart = priceEnd;
                        model.PossibleExchangeCar.PriceEnd = priceStart;
                    }

                    model.AdvertStatus = AdvertStatus.AddingPhotosProccess;
                    model.isPublished = false;
                    model.DateAdded = DateTime.Now;
                    model.DateUpdated = DateTime.Now;
                    model.UserId = user.UserId;

                    _advertReposity.Add(model);
                    _advertReposity.Save();

                    return model;
                }
            }

            return null;
        }

        public void RemoveAdvert(int userId, int advertId)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var advert = _advertReposity.Get(x => x.AdvertId == advertId && x.UserId == user.UserId);

                if (advert != null)
                {
                    var userAdvert = UserInAdvert(user.UserId, advert.AdvertId);

                    if (userAdvert != null)
                    {
                        var removedAdverts = _advertCountViewRepository.GetMany(x => x.AdvertId == userAdvert.AdvertId);
                        var exchangeCar = _possibleExchangeRepository.Get(x => x.PossibleExchangeCarId == advert.PossibleExchangeCarId);

                        // to remove 
                        var removedPhotosInAdvert = _photoInAdvertRepository.GetMany(x => x.AdvId == advert.AdvertId);

                        if (removedAdverts != null && removedAdverts.ToList().Count != 0)
                        {
                            foreach (var adv in removedAdverts)
                            {
                                _advertCountViewRepository.Delete(adv);
                            }

                            _advertCountViewRepository.Save();
                        }
                        _advertReposity.Delete(userAdvert);
                        _advertReposity.Save();

                        if (exchangeCar != null)
                        {
                            try
                            {
                                _possibleExchangeRepository.Delete(exchangeCar);
                                _possibleExchangeRepository.Save();
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        RemovePhotosByAdvertId(removedPhotosInAdvert);
                    }
                }
            }
        }

        private void RemovePhotosByAdvertId(IEnumerable<PhotoesInAdvert> advertPhotos)
        {
            if (advertPhotos.ToList().Count != 0)
            {
                foreach (var photo in advertPhotos)
                {
                    var deletedPhoto = _advertPhotoRepository.Get(x => x.AdvertPhotoId == photo.PhotoId);
                    if (deletedPhoto != null)
                    {
                        _advertPhotoRepository.Delete(deletedPhoto);

                    }

                    _advertPhotoRepository.Save();
                }
            }
        }

        public SearchAdvertViewModel SearchAdvert()
        {
            SearchAdvertViewModel model = new SearchAdvertViewModel();
            return model;
        }

        public IEnumerable<AdvertViewModel> GetSearchAdvertResult(SearchAdvertViewModel model)
        {
            if (model != null)
            {
                var searchResult = _advertReposity.GetMany(x => x.isPublished == true);

                foreach (var advert in searchResult)
                {
                    advert.Brand = _brandRepository.Get(x => x.BrandId == advert.BrandId);
                    advert.Model = _modelRepository.Get(x => x.ModelId == advert.ModelId);
                    advert.ReleaseYear = _releaseYearRepository.Get(x => x.ReleaseYearId == advert.ReleaseYearId);
                    advert.Engine = _engineRepository.Get(x => x.EngineId == advert.EngineId);
                    advert.Fuel = _fuelRepository.Get(x => x.FuelId == advert.FuelId);
                    advert.Transmission = _transmissionRepository.Get(x => x.TransmissionId == advert.TransmissionId);
                    advert.BodyType = _bodyTypeRepository.Get(x => x.BodyTypeId == advert.BodyTypeId);
                    advert.Region = _regionRepository.Get(x => x.RegionId == advert.RegionId);
                    advert.Registration = _registrationRepository.Get(x => x.RegistrationId == advert.RegistrationId);
                    advert.Condition = _conditionRepository.Get(x => x.ConditionId == advert.ConditionId);
                }

                // Brand
                if (model.BrandId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.BrandId == model.BrandId);
                }

                // Model
                if (model.ModelId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.ModelId == model.ModelId);
                }

                // Release Year
                if (model.ReleaseYearId_From.HasValue && model.ReleaseYearId_To.HasValue)
                {
                    var releaseYearFrom = _releaseYearRepository.Get(x => x.ReleaseYearId == model.ReleaseYearId_From).Year;
                    var releaseYearTo = _releaseYearRepository.Get(x => x.ReleaseYearId == model.ReleaseYearId_To).Year;

                    if (releaseYearFrom < releaseYearTo)
                    {
                        searchResult = searchResult.Where(x => x.ReleaseYear.Year >= releaseYearFrom &&
                                                               x.ReleaseYear.Year <= releaseYearTo);
                    }
                    if (releaseYearFrom > releaseYearTo)
                    {
                        searchResult = searchResult.Where(x => x.ReleaseYear.Year >= releaseYearTo &&
                                                               x.ReleaseYear.Year <= releaseYearFrom);
                    }
                    if (releaseYearTo == releaseYearFrom)
                    {
                        searchResult = searchResult.Where(x => x.ReleaseYear.Year == releaseYearTo);
                    }
                }

                if (model.ReleaseYearId_From.HasValue && model.ReleaseYearId_To == null)
                {
                    var releaseYearFrom = _releaseYearRepository.Get(x => x.ReleaseYearId == model.ReleaseYearId_From).Year;
                    searchResult = searchResult.Where(x => x.ReleaseYear.Year >= releaseYearFrom);
                }

                if (model.ReleaseYearId_From == null && model.ReleaseYearId_To.HasValue)
                {
                    var releaseYearTo = _releaseYearRepository.Get(x => x.ReleaseYearId == model.ReleaseYearId_To).Year;
                    searchResult = searchResult.Where(x => x.ReleaseYear.Year <= releaseYearTo);
                }

                // Mileage
                if (model.Mileage_From.HasValue && model.Mileage_To.HasValue)
                {
                    if (model.Mileage_From < model.Mileage_To)
                    {
                        searchResult = searchResult.Where(x => x.Mileage >= model.Mileage_From &&
                                                               x.Mileage <= model.Mileage_To);
                    }

                    if (model.Mileage_From > model.Mileage_To)
                    {
                        searchResult = searchResult.Where(x => x.Mileage >= model.Mileage_To &&
                                                               x.Mileage <= model.Mileage_From);
                    }

                    if (model.Mileage_From == model.Mileage_To)
                    {
                        searchResult = searchResult.Where(x => x.Mileage == model.Mileage_From);
                    }
                }
                if (model.Mileage_From.HasValue && model.Mileage_To == null)
                {
                    searchResult = searchResult.Where(x => x.Mileage >= model.Mileage_From);
                }
                if (model.Mileage_From == null && model.Mileage_To.HasValue)
                {
                    searchResult = searchResult.Where(x => x.Mileage <= model.Mileage_To);
                }
                // Engine
                if (model.EngineId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.EngineId == model.EngineId);
                }
                // Fuel
                if (model.FuelId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.FuelId == model.FuelId);
                }
                // Transmission
                if (model.TransmissionId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.TransmissionId == model.TransmissionId);
                }
                // BodyType
                if (model.BodyTypeId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.BodyTypeId == model.BodyTypeId);
                }
                // Region
                if (model.RegionId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.RegionId == model.RegionId);
                }
                // Registration
                if (model.RegistrationId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.RegistrationId == model.RegistrationId);
                }
                // Condition
                if (model.ConditionId.HasValue)
                {
                    searchResult = searchResult.Where(x => x.ConditionId == model.ConditionId);
                }

                searchResult.OrderBy(x => x.DateUpdated);

                List<AdvertViewModel> dest = new List<AdvertViewModel>();
                _mapper.Map(searchResult, dest);

                return dest;
            }

            return null;
        }

        public IEnumerable<AdvertViewModel> GetAdvertsByBrand(int brandId)
        {
            var brand = _brandRepository.Get(x => x.BrandId == brandId);

            if (brand != null)
            {
                var result = _advertReposity.GetMany(x => x.BrandId == brand.BrandId)
                                             .Where(x => x.isPublished == true)
                                            .OrderBy(x => x.DateUpdated).ToList();

                List<AdvertViewModel> dest = new List<AdvertViewModel>();

                if (result.Count != 0 || result != null)
                {
                    foreach (var advert in result)
                    {
                        advert.Brand = _brandRepository.Get(x => x.BrandId == advert.BrandId);
                        advert.Model = _modelRepository.Get(x => x.ModelId == advert.ModelId);
                        advert.ReleaseYear = _releaseYearRepository.Get(x => x.ReleaseYearId == advert.ReleaseYearId);
                        advert.Engine = _engineRepository.Get(x => x.EngineId == advert.EngineId);
                        advert.Fuel = _fuelRepository.Get(x => x.FuelId == advert.FuelId);
                        advert.Transmission = _transmissionRepository.Get(x => x.TransmissionId == advert.TransmissionId);
                        advert.BodyType = _bodyTypeRepository.Get(x => x.BodyTypeId == advert.BodyTypeId);
                        advert.Region = _regionRepository.Get(x => x.RegionId == advert.RegionId);
                        advert.Registration = _registrationRepository.Get(x => x.RegistrationId == advert.RegistrationId);
                        advert.Condition = _conditionRepository.Get(x => x.ConditionId == advert.ConditionId);
                    }

                    _mapper.Map(result, dest);
                }

                _mapper.Map(result, dest);

                return dest;
            }

            return null;
        }

        public IEnumerable<AdvertViewModel> GetAdvertsByRegion(int regionId)
        {
            var region = _regionRepository.Get(x => x.RegionId == regionId);

            if (region != null)
            {
                var result = _advertReposity.GetMany(x => x.RegionId == region.RegionId)
                                            .Where(x => x.isPublished == true)
                                            .OrderBy(x => x.DateUpdated).ToList();

                List<AdvertViewModel> dest = new List<AdvertViewModel>();

                if (result.Count != 0 || result != null)
                {
                    foreach (var advert in result)
                    {
                        advert.Brand = _brandRepository.Get(x => x.BrandId == advert.BrandId);
                        advert.Model = _modelRepository.Get(x => x.ModelId == advert.ModelId);
                        advert.ReleaseYear = _releaseYearRepository.Get(x => x.ReleaseYearId == advert.ReleaseYearId);
                        advert.Engine = _engineRepository.Get(x => x.EngineId == advert.EngineId);
                        advert.Fuel = _fuelRepository.Get(x => x.FuelId == advert.FuelId);
                        advert.Transmission = _transmissionRepository.Get(x => x.TransmissionId == advert.TransmissionId);
                        advert.BodyType = _bodyTypeRepository.Get(x => x.BodyTypeId == advert.BodyTypeId);
                        advert.Region = _regionRepository.Get(x => x.RegionId == advert.RegionId);
                        advert.Registration = _registrationRepository.Get(x => x.RegistrationId == advert.RegistrationId);
                        advert.Condition = _conditionRepository.Get(x => x.ConditionId == advert.ConditionId);
                    }

                    _mapper.Map(result, dest);
                }

                _mapper.Map(result, dest);

                return dest;
            }

            return null;
        }

        public IList<UserAdvertViewModel> GetUserAdverts(int userId)
        {
            var daysUpdate = 5;
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var userAdverts = _advertReposity.GetMany(x =>
                                                             x.UserId == user.UserId).OrderBy(
                                                             x => x.DateUpdated).ToList();

                if (userAdverts != null)
                {
                    List<UserAdvertViewModel> result = new List<UserAdvertViewModel>();

                    foreach (var advert in userAdverts)
                    {
                        var nearestDayUpdate = string.Empty;
                        bool isCanUpdate = false;

                        var updateDaysLeft = (DateTime.Now.Date - advert.DateUpdated.Date);

                        if (updateDaysLeft.Days >= daysUpdate)
                        {
                            isCanUpdate = true;
                        }
                        else
                        {
                            var differentInDays = daysUpdate - updateDaysLeft.Days;
                            var culture = new System.Globalization.CultureInfo("ru-RU");

                            nearestDayUpdate = GetRussianStringDateToUpdate(updateDaysLeft.Days);
                        }

                        result.Add(new UserAdvertViewModel
                        {
                            AdvertId = advert.AdvertId,
                            Brand = _brandRepository.Get(x => x.BrandId == advert.BrandId),
                            Model = _modelRepository.Get(x => x.ModelId == advert.ModelId),
                            Price = advert.Price,
                            ReleaseYear = _releaseYearRepository.Get(x => x.ReleaseYearId == advert.ReleaseYearId),
                            Mileage = advert.Mileage,
                            Engine = _engineRepository.Get(x => x.EngineId == advert.EngineId),
                            Fuel = _fuelRepository.Get(x => x.FuelId == advert.FuelId),
                            AdvertStatus = advert.AdvertStatus,
                            isPublish = advert.isPublished,
                            isCanUpdate = isCanUpdate,
                            NearestDateUpdate = nearestDayUpdate,
                            DateAdded = advert.DateAdded.Date,
                            DateUpdated = advert.DateUpdated,
                            Views = GetViewCountAdvert(advert.AdvertId)
                        });
                    }

                    return result;
                }
            }

            return null;
        }

        private string GetRussianStringDateToUpdate(int days)
        {
            var daysUpdate = 5;
            var differentInDays = daysUpdate - days;
            var culture = new System.Globalization.CultureInfo("ru-RU");
            DateTime nowDate = DateTime.Now;

            var dayOfWeek = culture.DateTimeFormat.GetDayName(nowDate.AddDays(differentInDays).DayOfWeek);
            var day = nowDate.AddDays(differentInDays).Day;
            var month = culture.DateTimeFormat.GetMonthName(nowDate.AddDays(differentInDays).Month);
            var year = nowDate.AddDays(differentInDays).Year;

            var result = string.Format("{0}, {1} {2} {3}", dayOfWeek, day, month, year);
            return result;
        }

        // TODO : Если это обьявление которое разместил юзер который сейчас смотрит его, проверить какой у него статус и редирект на страницу
        public AdvertViewModel GetAdvert(int advertId, int? userWhoLookedId)
        {
            var advert = _advertReposity.Get(x => x.AdvertId == advertId);

            var userWhoLooked = _userRepository.Get(x => x.UserId == userWhoLookedId);

            if (advert != null)
            {
                advert.BodyType = _bodyTypeRepository.Get(x => x.BodyTypeId == advert.BodyTypeId);
                advert.Brand = _brandRepository.Get(x => x.BrandId == advert.BrandId);
                advert.Condition = _conditionRepository.Get(x => x.ConditionId == advert.ConditionId);
                advert.Engine = _engineRepository.Get(x => x.EngineId == advert.EngineId);
                advert.Fuel = _fuelRepository.Get(x => x.FuelId == advert.FuelId);
                advert.Model = _modelRepository.Get(x => x.ModelId == advert.ModelId);
                advert.Region = _regionRepository.Get(x => x.RegionId == advert.RegionId);
                advert.Registration = _registrationRepository.Get(x => x.RegistrationId == advert.RegistrationId);
                advert.ReleaseYear = _releaseYearRepository.Get(x => x.ReleaseYearId == advert.ReleaseYearId);
                advert.Transmission = _transmissionRepository.Get(x => x.TransmissionId == advert.TransmissionId);
                advert.User = _userRepository.Get(x => x.UserId == advert.UserId);

                AdvertViewModel dest = new AdvertViewModel();

                if (userWhoLooked != null)
                {
                    if (advert.UserId != userWhoLooked.UserId)
                    {
                        if (advert.isPublished != true)
                        {
                            return null;
                        }

                        var advertInView = _advertCountViewRepository.Get(x => x.AdvertId == advert.AdvertId);

                        if (advertInView != null)
                        {
                            advertInView.Count = advertInView.Count + 1;
                            _advertCountViewRepository.Save();
                        }
                        else
                        {
                            _advertCountViewRepository.Add(new AdvertCountView
                            {
                                AdvertId = advert.AdvertId,
                                Count = 1,
                            });

                            _advertCountViewRepository.Save();
                        }

                        _mapper.Map(advert, dest);
                        dest.CountViews = GetViewCountAdvert(dest.AdvertId);
                        dest.Exchanges = _exchangeService.GetExchanges(dest.AdvertId);
                        return dest;
                    }
                }
                else
                {
                    if (advert.isPublished != true)
                    {
                        return null;
                    }

                    var advertInView = _advertCountViewRepository.Get(x => x.AdvertId == advert.AdvertId);

                    if (advertInView != null)
                    {
                        advertInView.Count = advertInView.Count + 1;
                        _advertCountViewRepository.Save();
                    }
                }

                _mapper.Map(advert, dest);
                dest.CountViews = GetViewCountAdvert(dest.AdvertId);
                dest.Exchanges = _exchangeService.GetExchanges(dest.AdvertId);

                return dest;
            }

            return null;
        }

        private int GetViewCountAdvert(int advertId)
        {
            var advert = _advertCountViewRepository.Get(x => x.AdvertId == advertId);

            if (advert != null)
            {
                return advert.Count;
            }

            return 0;
        }

        public Advert EditAdvert(int userId, Advert model)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var advert = UserInAdvert(user.UserId, model.UserId);

                if (advert != null && advert.isPublished == true)
                {
                    advert.BodyTypeId = model.BodyTypeId;
                    advert.BrandId = model.BrandId;
                    advert.ConditionId = model.ConditionId;
                    advert.Description = model.Description;
                    advert.EngineId = model.EngineId;
                    advert.FuelId = model.FuelId;
                    advert.Mileage = model.Mileage;
                    advert.ModelId = model.ModelId;
                    advert.Price = model.Price;
                    advert.RegionId = model.RegionId;
                    advert.RegistrationId = model.RegistrationId;
                    advert.ReleaseYearId = model.ReleaseYearId;
                    advert.TransmissionId = model.TransmissionId;

                    _advertReposity.Update(advert);
                    _advertReposity.Save();

                    return advert;
                }
            }

            return null;
        }

        public Advert UpdateAdvertDateAdded(int userId, int advertId)
        {
            var daysUpdate = 5;
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var advert = UserInAdvert(user.UserId, advertId);

                if (advert != null && advert.isPublished == true)
                {
                    var updateDaysLeft = (DateTime.Now.Date - advert.DateUpdated.Date).Days;

                    if (updateDaysLeft >= daysUpdate)
                    {
                        advert.DateUpdated = DateTime.Now;

                        _advertReposity.Update(advert);
                        _advertReposity.Save();
                    }
                    else
                    {
                        return null;
                    }

                    return advert;
                }
            }

            return null;
        }

        // TODO : Need add not authorize user which can add adv to favourite
        public int? AddAdvertToFavourite(int userId, int advertId)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var advert = _advertReposity.Get(x => x.AdvertId == advertId && x.isPublished == true);

                if (advert != null)
                {
                    var userFavouriteAdvert = _favouriteAdvertRepository.Get(x => x.UserId == user.UserId &&
                                                                                  x.AdvertId == advert.AdvertId);
                    if (userFavouriteAdvert == null)
                    {
                        _favouriteAdvertRepository.Add(new FavouriteAdvert
                        {
                            UserId = user.UserId,
                            AdvertId = advert.AdvertId,
                            DateAdded = DateTime.Now
                        });

                        _favouriteAdvertRepository.Save();

                        return 1;
                    }

                    return null;
                }
            }

            return null;
        }

        // TODO : Need add not authorize user which can add adv to favourite
        public IEnumerable<FavouriteAdvert> GetFavouriteAdverts(int userId)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var userFavouriteAdverts = _favouriteAdvertRepository.GetMany(x => x.UserId == user.UserId).ToList();

                if (userFavouriteAdverts != null || userFavouriteAdverts.Count != 0)
                {
                    return userFavouriteAdverts;
                }
            }

            return null;
        }

        // TODO : Need add not authorize user which can add adv to favourite
        public void RemoveAdvertFromFavourites(int userId, int advertId)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                var advert = _advertReposity.Get(x => x.AdvertId == advertId);

                if (advert != null)
                {
                    var removedFavouriteAdvert = _favouriteAdvertRepository.Get(x => x.AdvertId == advert.AdvertId &&
                                                                                   x.UserId == user.UserId);

                    if (removedFavouriteAdvert != null)
                    {
                        _favouriteAdvertRepository.Delete(removedFavouriteAdvert);
                        _favouriteAdvertRepository.Save();
                    }
                }
            }
        }

        private Advert UserInAdvert(int userId, int? advertId)
        {
            var advert = _advertReposity.Get(x =>
                                                 x.AdvertId == advertId &&
                                                 x.UserId == userId);
            if (advert != null)
            {
                return advert;
            }

            return null;
        }
    }
}