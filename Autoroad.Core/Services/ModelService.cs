﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
    public class ModelService : IModelService
    {
        private readonly IModelRepository _modelRepository;

        public ModelService(IModelRepository modelRepository)
        {
            _modelRepository = modelRepository;
        }

        public IEnumerable<Model> GetModels()
        {
            return _modelRepository.GetAll();
        }

        public IList<Model> GetModels(int id)
        {
            return _modelRepository.GetMany(m => m.BrandId == id).ToList();
        }
    }
}
