﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;


namespace Autoroad.Core.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAdvertRepository _advertReposity;

        public UserService(IUserRepository userRepository, IAdvertRepository advertRepository)
        {
            _userRepository = userRepository;
            _advertReposity = advertRepository;
        }

        public User GetUserPersonalData(int userId)
        {
            var user = _userRepository.Get(x => x.UserId == userId);

            if (user != null)
            {
                return user;
            }

            return null;
        }

        public User EditUserPersonalData(User model)
        {
            var user = _userRepository.Get(x => x.UserId == model.UserId);

            if (user != null)
            {
                user.Phone = model.Phone;
                user.ContactName = model.ContactName;
                user.City = model.City;
                user.VKContact = model.VKContact;
                user.SkypeContact = model.SkypeContact;
                user.FacebookContact = model.FacebookContact;
                user.IsShowEmail = model.IsShowEmail;
                user.IsNotifyAuction = model.IsNotifyAuction;
                user.IsNotifyExchange = model.IsNotifyExchange;
                user.IsShowCity = model.IsShowCity;
                user.IsShowFacebook = model.IsShowFacebook;
                user.IsShowSkype = model.IsShowSkype;
                user.IsShowVK = model.IsShowVK;
                

                _userRepository.Update(user);
                _userRepository.Save();

                return user;
            }

            return null;
        }
    }
}
