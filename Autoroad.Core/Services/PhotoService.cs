﻿using Autoroad.Core.IServices;
using Autoroad.Core.ViewModels;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace Autoroad.Core.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly IAdvertPhotoRepository _photoRepository;
        private readonly IPhotoInAdvertRepository _photoInAdvertRepository;
        private readonly IAdvertRepository _advertRepository;

        public PhotoService(IAdvertRepository advertRepository, IAdvertPhotoRepository photoRepository, IPhotoInAdvertRepository photoInAdvertRepository)
        {
            _advertRepository = advertRepository;
            _photoRepository = photoRepository;
            _photoInAdvertRepository = photoInAdvertRepository;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public string GetNewFileName(HttpPostedFile file)
        {
            var mimeType = MimeMapping.GetMimeMapping(file.FileName);
            var fileSize = Math.Round(((decimal)file.ContentLength / (decimal)1024), 2);
            if (!mimeType.StartsWith("image/") || fileSize > 8000)
            {
                return null;
            }

            var imageFormat = mimeType.IndexOf('/');
            var gettingImageFormat = mimeType.ToString().Substring(imageFormat);
            var finalImageFormat = gettingImageFormat.Replace('/', '.');

            var newFileName = Guid.NewGuid().ToString() + finalImageFormat;

            return newFileName;
        }

        public void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath, string newFileName, int advertId)
        {
            var photoPath = string.Format("~/Uploads/{0}", newFileName);

            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);

                var uploadedPhoto = _photoRepository.Add(new AdvertPhoto { PhotoPath = photoPath });
                _photoRepository.Save();

                _photoInAdvertRepository.Add(new PhotoesInAdvert { AdvId = advertId, PhotoId = uploadedPhoto.AdvertPhotoId });
                _photoInAdvertRepository.Save();

                ChangeAdvertStatus(advertId);

            }
        }

        public void SavePNGFile(HttpPostedFile file, string fname, string newFileName, int advertId)
        {
            var photoPath = string.Format("~/Uploads/{0}", newFileName);
            file.SaveAs(fname);
            var uploadedPhoto = _photoRepository.Add(new AdvertPhoto { PhotoPath = photoPath });
            _photoRepository.Save();

            _photoInAdvertRepository.Add(new PhotoesInAdvert { AdvId = advertId, PhotoId = uploadedPhoto.AdvertPhotoId });
            _photoInAdvertRepository.Save();

            ChangeAdvertStatus(advertId);
        }

        private void ChangeAdvertStatus(int advertId)
        {
            var advert = _advertRepository.Get(x => x.AdvertId == advertId);

            if (advert != null)
            {
                advert.AdvertStatus = AdvertStatus.ConfirmationAddingProccess;
                _advertRepository.Save();
            }
        }

        public IEnumerable<ExistingPhotoViewModel> GetExistingPhotos()
        {
            //Get the images list from repository
            var attachmentsList = new List<ExistingPhotoViewModel>
            {
                new ExistingPhotoViewModel {PhotoId = 2, FileName = "5c021a7e-3338-468b-b9fb-30e8f16bc98b.jpeg", Path = "/Uploads/5c021a7e-3338-468b-b9fb-30e8f16bc98b.jpeg"}
            }.ToList();

            return attachmentsList;
        }
    }
}
