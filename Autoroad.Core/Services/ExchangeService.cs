﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
    public class ExchangeService : IExchangeService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAdvertRepository _advertRepository;
        private readonly IExchangeRepository _exchangeRepository;

        public ExchangeService(IUserRepository userRepository, IAdvertRepository advertRepository, IExchangeRepository exchangeRepository)
        {
            _userRepository = userRepository;
            _advertRepository = advertRepository;
            _exchangeRepository = exchangeRepository;
        }

        public IEnumerable<Exchange> ExchangeOffer(int advertId, int exchangeAdvertId, string Description, int userId)
        {
            var advert = _advertRepository.Get(x => x.AdvertId == advertId);

            if (advert != null)
            {
                var user = _userRepository.Get(x => x.UserId == userId);

                if (user != null)
                {
                    var exchangeAdv = _advertRepository.Get(x => x.AdvertId == exchangeAdvertId);

                    if (exchangeAdv != null)
                    {
                        var offer = IsUserExchangedOfferToAdvert(advert.AdvertId, exchangeAdv.AdvertId);

                        if (!offer)
                        {
                            // TODO : if user exchange offer to him self, its imposible, need to fix
                            _exchangeRepository.Add(new Exchange
                            {
                                AdvertId = advert.AdvertId,
                                ExchangingUserId = user.UserId,
                                ExchengingAdvertId = exchangeAdv.AdvertId,
                                Description = Description,
                                DateExchanging = DateTime.Now
                            });

                            _exchangeRepository.Save();

                            var result = _exchangeRepository.GetMany(x => x.AdvertId == advert.AdvertId);
                            return result;
                        }
                    }
                }
            }

            return null;
        }

        public IEnumerable<Exchange> GetExchanges(int advertId)
        {
            var advert = _advertRepository.Get(x => x.AdvertId == advertId);

            if (advert != null)
            {
                var result = _exchangeRepository.GetMany(x => x.AdvertId == advert.AdvertId);

                return result;
            }

            return null;
        }

        private bool IsUserExchangedOfferToAdvert(int advertId, int exchangeAdvertId)
        {
            var dbExchanges = _exchangeRepository.Get(x => x.AdvertId == advertId &&
                                                           x.ExchengingAdvertId == exchangeAdvertId);

            if (dbExchanges != null)
            {
                return true;
            }

            return false;
        }
    }
}
