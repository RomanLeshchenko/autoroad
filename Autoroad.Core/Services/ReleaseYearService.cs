﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
    public class ReleaseYearService : IReleaseYearService
    {
        private readonly IReleaseYearRepository _releaseYearRepository;

        public ReleaseYearService(IReleaseYearRepository releaseYearRepository)
        {
            _releaseYearRepository = releaseYearRepository;
        }

        public IEnumerable<ReleaseYear> GetReleaseYears()
        {
            return _releaseYearRepository.GetAll();
        }
    }
}
