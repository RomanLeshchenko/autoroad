﻿using Autoroad.Core.IServices;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.Services
{
    public class BodyTypeService : IBodyTypeService
    {
        private readonly IBodyTypeRepository _bodyTypeRepository;

        public BodyTypeService(IBodyTypeRepository bodyTypeRepository)
        {
            _bodyTypeRepository = bodyTypeRepository;
        }

        public IEnumerable<BodyType> GetBodyTypes()
        {
            return _bodyTypeRepository.GetAll();
        }
    }
}
