﻿using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.ViewModels
{
    public class AdvertViewModel
    {
        public int AdvertId { get; set; }
        public int BrandId { get; set; }
        public int ModelId { get; set; }
        public int Price { get; set; }
        public int ReleaseYearId { get; set; }
        public int Mileage { get; set; }
        public int EngineId { get; set; }
        public int FuelId { get; set; }
        public int TransmissionId { get; set; }
        public int BodyTypeId { get; set; }
        public int RegionId { get; set; }
        public int RegistrationId { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public int ConditionId { get; set; }
        public int CountViews { get; set; }

        public Condition Condition { get; set; }
        public User User { get; set; }
        public Registration Registration { get; set; }
        public Region Region { get; set; }
        public BodyType BodyType { get; set; }
        public Transmission Transmission { get; set; }
        public Fuel Fuel { get; set; }
        public Engine Engine { get; set; }
        public ReleaseYear ReleaseYear { get; set; }
        public Model Model { get; set; }
        public Brand Brand { get; set; }
        public IEnumerable<Exchange> Exchanges { get; set; }


        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
