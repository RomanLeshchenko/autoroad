﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Core.ViewModels
{
    public class ExistingPhotoViewModel
    {
        public long PhotoId { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
    }
}
