﻿using Autoroad.Data.Models;
using System.Collections.Generic;

namespace Autoroad.Data.ViewModels
{
    public class SearchAdvertViewModel
    {
        public Brand Brand { get; set; }
        public int? BrandId { get; set; }
        public Model Model { get; set; }
        public int? ModelId { get; set; }
        public int? ReleaseYearId_From { get; set; }
        public int? ReleaseYearId_To { get; set; }
        public int? Mileage_From { get; set; }
        public int? Mileage_To { get; set; }
        public Engine Engine { get; set; }
        public int? EngineId { get; set; }
        public Fuel Fuel { get; set; }
        public int? FuelId { get; set; }
        public Transmission Transmission { get; set; }
        public int? TransmissionId { get; set; }
        public BodyType BodyType { get; set; }
        public int? BodyTypeId { get; set; }
        public Region Region { get; set; }
        public int? RegionId { get; set; }
        public Registration Registration { get; set; }
        public int? RegistrationId { get; set; }
        public Condition Condition { get; set; }
        public int? ConditionId { get; set; }
    }
}
