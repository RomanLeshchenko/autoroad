﻿using Autoroad.Data.Models;
using System;

namespace Autoroad.Data.ViewModels
{
    public class UserAdvertViewModel
    {
        public int AdvertId { get; set; }
        public Brand Brand { get; set; }
        public Model Model { get; set; }
        public ReleaseYear ReleaseYear { get; set; }
        public int Price { get; set; }
        public int Mileage { get; set; }
        public Fuel Fuel { get; set; }
        public Engine Engine { get; set; }
        public AdvertStatus? AdvertStatus { get; set; }
        public bool isPublish { get; set; }
        public bool isCanUpdate { get; set; }
        public string NearestDateUpdate { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
        public int Views { get; set; }
    }
}
