﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Autoroad.Data.Models
{
    public class User
    {
        [Key, DatabaseGeneratedAttribute(DatabaseGeneratedOption.None), Required]
        public int UserId { get; set; }
        [Required]
        [Display(Name = "Контактное имя")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Контактное имя может содержать только буквы!")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Контактное имя должно иметь хотя бы 2 буквы и максимум 20")]
        public string ContactName { get; set; }
        [Required]
        [Display(Name = "Телефон")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Номер не может включать в себя другие символы кроме цифр!")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Введите номер в таком формате: 80971234567 ")]
        public string Phone { get; set; }
        [Required]
        [Display(Name = "Город")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Город может содержать только буквы!")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Город должен иметь хотя бы 2 буквы и максимум 20")]
        public string City { get; set; }
        [StringLength(100, ErrorMessage = "Логин не может быть длиннее 100 символов.", MinimumLength = 5)]
        public string Login { get; set; }
        public DateTime DateRegister { get; set; }
        public string SkypeContact { get; set; }
        public string VKContact { get; set; }
        public string FacebookContact { get; set; }

        [Display(Name = "Показывать пользователям мой E-mail")]
        public bool IsShowEmail { get; set; }
        [Display(Name = "Показывать пользователям мой город")]
        public bool IsShowCity { get; set; }
        [Display(Name = "Показывать пользователям мой Skype")]
        public bool IsShowSkype { get; set; }
        [Display(Name = "Показывать пользователям мой аккаунт Вконтакте")]
        public bool IsShowVK { get; set; }
        [Display(Name = "Показывать пользователям мой аккаунт Facebook")]
        public bool IsShowFacebook { get; set; }
        [Display(Name = "Оповещать меня об предложении торга")]
        public bool IsNotifyAuction { get; set; }
        [Display(Name = "Оповещать меня об предложении обмена")]
        public bool IsNotifyExchange { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<FavouriteAdvert> FavouriteAdverts { get; set; }
    }
}