﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoroad.Data.Models
{
    public class BodyType
    {
        [Key]
        public int BodyTypeId { get; set; }
        [Required]
        [Display(Name = "Тип кузова")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Поле может содержать только буквы")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Поле должно иметь хотя бы 2 буквы и максимум 20")]
        public string Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<PossibleExchangeCar> PossibleCar { get; set; }
    }
}