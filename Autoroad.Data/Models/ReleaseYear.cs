﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Autoroad.Data.Models
{
    public class ReleaseYear
    {
        [Key]
        public int ReleaseYearId { get; set; }
        [Required]
        [Display(Name="Год выпуска")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Год не может включать в себя другие символы кроме цифр!")]
        [Range(1910,2015,ErrorMessage="Минимальный год : 1910, Максимальный год : 2016")]
        public int Year { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
    }
}