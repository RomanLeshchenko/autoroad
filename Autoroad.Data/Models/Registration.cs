﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Autoroad.Data.Models
{
    public class Registration
    {
        [Key]
        public int RegistrationId { get; set; }
        [Required]
        [Display(Name="Регистрация")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Регистрация может содержать только буквы!")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Регистрация должна иметь хотя бы 2 буквы и максимум 20")]
        public string Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
    }
}