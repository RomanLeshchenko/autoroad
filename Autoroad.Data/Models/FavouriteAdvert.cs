﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
    public class FavouriteAdvert
    {
        public int FavouriteAdvertId { get; set; }

        public int UserId { get; set; }

        public int AdvertId { get; set; }

        public DateTime DateAdded { get; set; }

    }
}
