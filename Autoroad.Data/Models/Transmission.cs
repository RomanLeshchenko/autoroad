﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Autoroad.Data.Models
{
    public class Transmission
    {
        [Key]
        public int TransmissionId { get; set; }
        [Required]
        [Display(Name="КПП")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "КПП может содержать только буквы!")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "КПП должен иметь хотя бы 2 буквы и максимум 20")]
        public string Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<PossibleExchangeCar> PossibleCar { get; set; }
    }
}