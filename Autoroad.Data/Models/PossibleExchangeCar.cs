﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoroad.Data.Models
{
	public class PossibleExchangeCar
	{
		[Key]
		public int PossibleExchangeCarId { get; set; }

		[Display(Name = "Марка")]
		public int? BrandId { get; set; }

		[Display(Name = "Модель")]
		public int? ModelId { get; set; }

		[Display(Name = "Год от")]
		public int? YearStartId { get; set; }

		[Display(Name = "Год до")]
		public int? YearEndId { get; set; }

		[Display(Name = "Цена от")]
		[Range(1, 150000000, ErrorMessage = "Цена от 1$ до 150 млн.$")]
		[RegularExpression(@"^[0-9]+$", ErrorMessage = "Только цифры")]
		public int? PriceStart { get; set; }

		[Display(Name = "Цена до")]
		[Range(1, 150000000, ErrorMessage = "Цена от 1$ до 150 млн.$")]
		[RegularExpression(@"^[0-9]+$", ErrorMessage = "Только цифры")]
		public int? PriceEnd { get; set; }

		[Display(Name = "Кузов")]
		public int? BodyTypeId { get; set; }

		public int? BodyTypeTwoId { get; set; }

		[Display(Name = "КПП")]
		public int? TransmissionId { get; set; }

		[Display(Name = "Объем двигателя")]
		public int? EngineId { get; set; }

		public int? EngineTwoId { get; set; }

		[Display(Name = "Тип обмена")]
		public int? ExchangeTypeId { get; set; }

		public Engine Engine { get; set; }
		public ExchangeType ExchangeType { get; set; }

		public Transmission Transmission { get; set; }
		public BodyType BodyType { get; set; }
		public Brand Brand { get; set; }
		public Model Model { get; set; }

		public virtual ICollection<Advert> Adverts { get; set; }
	}
}