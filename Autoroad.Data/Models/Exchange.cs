﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
    public class Exchange
    {
        public int ExchangeId { get; set; }

        public int AdvertId { get; set; }

        public int ExchengingAdvertId { get; set; }

        public int ExchangingUserId { get; set; }

        public string Description { get; set; }

        public DateTime DateExchanging { get; set; }

    }
}
