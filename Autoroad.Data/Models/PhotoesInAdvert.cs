﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
    public class PhotoesInAdvert
    {
        [Key]
        public int PhotoInAdvertId { get; set; }
        
        public int AdvId { get; set; }
        
        public int PhotoId { get; set; }

        [ForeignKey("AdvId")]
        public Advert Advert { get; set; }
        [ForeignKey("PhotoId")]
        public AdvertPhoto AdvertPhoto { get; set; }
    }
}
