﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoroad.Data.Models
{
    public class Engine
    {
        [Key]
        public int EngineId { get; set; }
        [Required]
        [Display(Name = "Обьем двигателя")]
 
        public double Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<PossibleExchangeCar> PossibleCar { get; set; }
    }
}