﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
	public class ExchangeType
	{
		[Key]
		public int ExchangeTypeId { get; set; }

		[Display(Name = "Тип обмена")]
		public string TypeName { get; set; }
	}
}