﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
	public class DriveUnit
	{
		[Key]
		public int DriveUnitId { get; set; }

		[Required]
		[Display(Name = "Привод")]
		[RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Поле может содержать только буквы!")]
		public string Name { get; set; }

		public virtual ICollection<Advert> Adverts { get; set; }
	}
}