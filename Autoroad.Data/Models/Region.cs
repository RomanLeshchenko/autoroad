﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoroad.Data.Models
{
    public class Region
    {
        [Key]
        public int RegionId { get; set; }
        [Required]
        [Display(Name = "Область")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Поле может содержать только буквы!")]
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Поле должно иметь хотя бы 2 буквы и максимум 40")]
        public string Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
    }
}