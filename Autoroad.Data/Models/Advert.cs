﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Autoroad.Data.Models
{
	public enum AdvertStatus
	{
		AddingPhotosProccess = 0,
		ConfirmationAddingProccess = 1,
        Published = 2
	}

	public class Advert
	{
		[Key]
		public int AdvertId { get; set; }

		[Required(ErrorMessage = "Выберите Марку")]
		[Display(Name = "Марка")]
		[ForeignKey("Brand")]
		public int BrandId { get; set; }

		[Required(ErrorMessage = "Выберите Модель")]
		[Display(Name = "Модель")]
		public int ModelId { get; set; }

		[Required(ErrorMessage = "Укажите Цену")]
		[Display(Name = "Цена")]
		[Range(1, 150000000, ErrorMessage = "Цена от 1$ до 150 млн.$")]
		[RegularExpression(@"^[0-9]+$", ErrorMessage = "Только цифры")]
		public int Price { get; set; }

		[Required(ErrorMessage = "Выберите Год")]
		[Display(Name = "Год выпуска")]
		[ForeignKey("ReleaseYear")]
		public int ReleaseYearId { get; set; }

		[Required(ErrorMessage = "Укажите Пробег")]
		[Display(Name = "Пробег")]
		[RegularExpression(@"^[0-9]+$", ErrorMessage = "Только цифры")]
		[Range(1, 10000000, ErrorMessage = "Пробег от 1 тыс. до 10 млн")]
		public int Mileage { get; set; }

		[Required(ErrorMessage = "Выберите Объем")]
		[Display(Name = "Обьем двигателя")]
		[ForeignKey("Engine")]
		public int EngineId { get; set; }

		[Required(ErrorMessage = "Выберите Топливо")]
		[Display(Name = "Топливо")]
		[ForeignKey("Fuel")]
		public int FuelId { get; set; }

		[Required(ErrorMessage = "Выберите КПП")]
		[Display(Name = "КПП")]
		[ForeignKey("Transmission")]
		public int TransmissionId { get; set; }

		[Required(ErrorMessage = "Выберите Кузов")]
		[Display(Name = "Тип кузова")]
		[ForeignKey("BodyType")]
		public int BodyTypeId { get; set; }

		[Required(ErrorMessage = "Выберите Область")]
		[Display(Name = "Область")]
		[ForeignKey("Region")]
		public int RegionId { get; set; }

		[Required(ErrorMessage = "Выберите Регистрацию")]
		[Display(Name = "Регистрация")]
		[ForeignKey("Registration")]
		public int RegistrationId { get; set; }

		[Display(Name = "Описание")]
		[StringLength(700, ErrorMessage = "Максимум 700 символов")]
		public string Description { get; set; }

		[ForeignKey("User")]
		public int UserId { get; set; }

		[Required(ErrorMessage = "Выберите Состояние")]
		[Display(Name = "Состояние")]
		[ForeignKey("Condition")]
		public int ConditionId { get; set; }

		// привод
		[Display(Name = "Привод")]
		public int? DriveUnitId { get; set; }

		public int? PossibleExchangeCarId { get; set; }

		[Display(Name = "Срочно!")]
		public bool isQuickly { get; set; }

		[Display(Name = "Не растаможен")]
		public bool isNotCustomClearance { get; set; }

		public AdvertStatus? AdvertStatus { get; set; }

		public bool isPublished { get; set; }

		#region Security

		/* ---- Security ---- */
		public bool ABD_security { get; set; }

		public bool ABS_security { get; set; }

		public bool ESP_security { get; set; }

		// бронированый автомобиль
		public bool Armor_security { get; set; }

		// галогенные фары
		public bool Galogen_security { get; set; }

		public bool KPP_security { get; set; }

		public bool Imobilaizer_security { get; set; }

		// пневмоподвеска
		public bool PnevmoTransmission_security { get; set; }

		public bool Airbag_security { get; set; }

		// серворуль
		public bool ServoRool_security { get; set; }

		// сигнализация
		public bool Signaling_security { get; set; }

		// центральный замок
		public bool CentralLock_security { get; set; }

		/* ---- Security ---- */

		#endregion Security

		#region Comfort

		/* ---- Comfort ---- */

		// бортовой компьютер
		public bool Computer_comfort { get; set; }

		// датчик света
		public bool LightSensor_comfort { get; set; }

		public bool ClimaControl_comfort { get; set; }

		// кожаный салон
		public bool Leather_comfort { get; set; }

		public bool Conditioner_comfort { get; set; }

		public bool CruiseControl_comfort { get; set; }

		public bool Luke_comfort { get; set; }

		public bool MultiRool_comfort { get; set; }

		// омыватель фар
		public bool HeadlightWasher_comfort { get; set; }

		public bool Parctronick_comfort { get; set; }

		// подогрев зеркал
		public bool HeatedMirrors_comfort { get; set; }

		// подогрев сидений
		public bool HeatedSeats_comfort { get; set; }

		// сенсор дождя
		public bool RainSensor_comfort { get; set; }

		// усилитель руля
		public bool PowerSteering_comfort { get; set; }

		// эл. стеклоподьемники
		public bool ElectroGlasses_comfort { get; set; }

		// эл. пакет
		public bool ElectroPackage_comfort { get; set; }

		/* ---- Comfort ---- */

		#endregion Comfort

		#region Multimedia

		/*Multimedia*/

		public bool CD_multimedia { get; set; }

		public bool DVD_multimedia { get; set; }

		public bool MP3_multimedia { get; set; }

		public bool Acoustic_multimedia { get; set; }

		public bool Magnitola_multimedia { get; set; }

		public bool SubWoofer_multimedia { get; set; }

		public bool GPS_multimedia { get; set; }

		/*Multimedia*/

		#endregion Multimedia

		#region Other

		/*Other*/

		public bool GBO_other { get; set; }

		public bool Tree_other { get; set; }

		public bool LongBase_other { get; set; }

		public bool RightRool_other { get; set; }

		// тонировка
		public bool Tinting_other { get; set; }

		public bool Farckop_other { get; set; }

		/*Other*/

		#endregion Other

		public DateTime DateAdded { get; set; }
		public DateTime DateUpdated { get; set; }

		// состояние
		public Condition Condition { get; set; }

		public User User { get; set; }
		public Registration Registration { get; set; }
		public Region Region { get; set; }
		public BodyType BodyType { get; set; }
		public Transmission Transmission { get; set; }
		public Fuel Fuel { get; set; }
		public Engine Engine { get; set; }
		public ReleaseYear ReleaseYear { get; set; }
		public Model Model { get; set; }
		public Brand Brand { get; set; }

		// возможен обмен?
		public bool IsPossibleExchange { get; set; }

		// возможен торг?
		public bool isPossibleAuction { get; set; }

		// возможен обмен на
		public PossibleExchangeCar PossibleExchangeCar { get; set; }

		// привод
		public DriveUnit DriveUnit { get; set; }

        public virtual ICollection<PhotoesInAdvert> PhotoesInAdverts { get; set; }
	}
}