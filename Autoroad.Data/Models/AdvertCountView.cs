﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
    public class AdvertCountView
    {
        public int AdvertCountViewId { get; set; }
        public int AdvertId { get; set; }
        public int Count { get; set; }
    }
}
