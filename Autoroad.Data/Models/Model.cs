﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Autoroad.Data.Models
{
    public class Model
    {
        [Key]
        public int ModelId { get; set; }
        [Required]
        [Display(Name = "Модель")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z0-9]+$", ErrorMessage = "Поле может содержать только буквы и цифры!")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Поле должно иметь хотя бы 2 символа и максимум 20")]
        public string Name { get; set; }
        [ForeignKey("Brand")]
        public int BrandId { get; set; }

        public Brand Brand { get; set; }
        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<PossibleExchangeCar> PossibleCar { get; set; }
    }
}