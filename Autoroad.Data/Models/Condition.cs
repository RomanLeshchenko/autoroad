﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Autoroad.Data.Models
{
    public class Condition
    {
        [Key]
        public int ConditionId { get; set; }
        [Required]
        [Display(Name = "Состояние")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Поле может содержать только буквы")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Поле должно иметь хотя бы 2 буквы и максимум 20")]
        public string Name { get; set; }

        public virtual ICollection<Advert> Adverts { get; set; }
    }
}