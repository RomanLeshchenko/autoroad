﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Models
{
    public class AdvertPhoto
    {
        [Key]
        public int AdvertPhotoId { get; set; }

        public string PhotoPath { get; set; }

        public virtual ICollection<PhotoesInAdvert> PhotoesInAdverts { get; set; }
    }
}
