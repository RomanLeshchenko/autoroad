﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Autoroad.Data.Models
{
    public class Brand
    {
        [Key]
        public int BrandId { get; set; }
        [Required]
        [Display(Name = "Марка")]
        [RegularExpression(@"^[а-яА-ЯёЁa-zA-Z0-9]+$", ErrorMessage = "Поле может содержать только буквы и цифры")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Поле должно иметь хотя бы 2 символа и максимум 20")]
        public string Name { get; set; }

        public virtual ICollection<Model> ModelAutos { get; set; }
        public virtual ICollection<Advert> Adverts { get; set; }
        public virtual ICollection<PossibleExchangeCar> PossibleCar { get; set; }
    }
}