﻿using Autoroad.Data.Models;
using System;
using System.Data.Entity;

namespace Autoroad.Data.DbContexts
{
    public class HomeContext : DbContext
    {
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<BodyType> BodyTypes { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<Engine> Engines { get; set; }
        public DbSet<Fuel> Fuels { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<ReleaseYear> ReleaseYears { get; set; }
        public DbSet<Transmission> Transmissions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AdvertCountView> AdvertCountViews { get; set; }
        public DbSet<FavouriteAdvert> FavouriteAdverts { get; set; }
        public DbSet<Exchange> Exchanges { get; set; }
        public DbSet<DriveUnit> DriveUnits { get; set; }
        public DbSet<PossibleExchangeCar> PossibleExchangeCars { get; set; }
        public DbSet<ExchangeType> ExchangeTypes { get; set; }
        public DbSet<AdvertPhoto> AdvertPhotoes { get; set; }
        public DbSet<PhotoesInAdvert> PhotoesInAdverts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<HomeContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}