﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface ITransmissionRepository : IBaseRepository<Transmission>
    {
    }
}
