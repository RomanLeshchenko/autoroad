﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
    }
}
