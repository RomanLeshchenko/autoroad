﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Autoroad.Data.IRepositories
{
    public interface IBaseRepository<T> where T : class
    {
        T Get(object id);
        T Get(Expression<Func<T, bool>> predicate);
        T Get(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);
        T One(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetMany(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes);
        T Add(T entity);
        T Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
