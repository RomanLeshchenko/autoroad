﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IReleaseYearRepository : IBaseRepository<ReleaseYear>
    {
    }
}
