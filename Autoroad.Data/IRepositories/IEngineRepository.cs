﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IEngineRepository : IBaseRepository<Engine>
    {
    }
}
