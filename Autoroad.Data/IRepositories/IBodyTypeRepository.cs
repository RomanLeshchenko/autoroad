﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IBodyTypeRepository : IBaseRepository<BodyType>
    {
    }
}
