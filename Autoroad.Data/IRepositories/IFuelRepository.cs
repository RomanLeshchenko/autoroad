﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IFuelRepository : IBaseRepository<Fuel>
    {
    }
}
