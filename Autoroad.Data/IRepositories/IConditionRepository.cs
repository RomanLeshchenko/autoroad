﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IConditionRepository : IBaseRepository<Condition>
    {
    }
}
