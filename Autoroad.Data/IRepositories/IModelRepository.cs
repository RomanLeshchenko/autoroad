﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IModelRepository : IBaseRepository<Model>
    {
    }
}
