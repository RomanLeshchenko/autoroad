﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IAdvertCountViewRepository : IBaseRepository<AdvertCountView>
    {
    }
}
