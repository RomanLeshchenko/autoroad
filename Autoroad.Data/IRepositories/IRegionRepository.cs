﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IRegionRepository : IBaseRepository<Region>
    {
    }
}
