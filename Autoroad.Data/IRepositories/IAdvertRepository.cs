﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IAdvertRepository : IBaseRepository<Advert>
    {
    }
}
