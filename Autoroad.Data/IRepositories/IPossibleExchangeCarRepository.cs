﻿using Autoroad.Data.Models;
using System;

namespace Autoroad.Data.IRepositories
{
    public interface IPossibleExchangeCarRepository : IBaseRepository<PossibleExchangeCar>
    {
    }
}
