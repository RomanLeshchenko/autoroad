﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IBrandRepository : IBaseRepository<Brand>
    {
    }
}
