﻿using Autoroad.Data.Models;

namespace Autoroad.Data.IRepositories
{
    public interface IFavouriteAdvertRepository : IBaseRepository<FavouriteAdvert>
    {
    }
}
