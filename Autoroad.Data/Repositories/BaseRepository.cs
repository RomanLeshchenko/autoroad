﻿using Autoroad.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Autoroad.Data.Repositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly DbContext _context;

        private readonly IDbSet<T> _dbset;

        protected BaseRepository(DbContext context)
        {
            _context = context;
            _dbset = context.Set<T>();
        }

        public virtual T Get(object id)
        {
            return _dbset.Find(id);
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _dbset.FirstOrDefault(predicate);
        }

        public virtual T Get(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] properties)
        {
            IQueryable<T> query = _dbset;

            if (properties != null)
            {
                query = properties.Aggregate(query, (current, property) => current.Include(property));
            }

            return query.Where(where).FirstOrDefault();
        }

        public T One(Expression<Func<T, bool>> predicate)
        {
            return _dbset.SingleOrDefault(predicate);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbset.ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> predicate)
        {
            return _dbset.Where(predicate).ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var query = _dbset.Where(predicate);

            query = includes.Aggregate(query, (current, include) => current.Include(include));

            return query.ToList();
        }

        public virtual T Add(T entity)
        {
            _dbset.Add(entity);
            return entity;
        }

        public virtual T Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }
    }
}
