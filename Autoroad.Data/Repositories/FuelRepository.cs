﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class FuelRepository : BaseRepository<Fuel>, IFuelRepository
    {
        public FuelRepository(HomeContext context)
            :base(context)
        {

        }
    }
}
