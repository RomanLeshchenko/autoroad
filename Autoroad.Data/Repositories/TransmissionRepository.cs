﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class TransmissionRepository : BaseRepository<Transmission>, ITransmissionRepository
    {
        public TransmissionRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
