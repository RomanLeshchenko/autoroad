﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Repositories
{
    public class AdvertPhotoRepository : BaseRepository<AdvertPhoto>, IAdvertPhotoRepository
    {
        public AdvertPhotoRepository(HomeContext context)
            : base(context)
        {

        }
    }

}
