﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Repositories
{
    public class PossibleExchangeCarRepository : BaseRepository<PossibleExchangeCar>, IPossibleExchangeCarRepository
    {
        public PossibleExchangeCarRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
