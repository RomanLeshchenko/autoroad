﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;


namespace Autoroad.Data.Repositories
{
    public class BodyTypeRepository : BaseRepository<BodyType>, IBodyTypeRepository
    {
        public BodyTypeRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
