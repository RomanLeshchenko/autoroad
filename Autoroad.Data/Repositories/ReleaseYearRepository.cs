﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class ReleaseYearRepository : BaseRepository<ReleaseYear>, IReleaseYearRepository
    {
        public ReleaseYearRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
