﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;


namespace Autoroad.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
