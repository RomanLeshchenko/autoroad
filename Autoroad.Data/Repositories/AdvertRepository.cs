﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class AdvertRepository : BaseRepository<Advert>, IAdvertRepository
    {
        public AdvertRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
