﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class ConditionRepository : BaseRepository<Condition>, IConditionRepository
    {
        public ConditionRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
