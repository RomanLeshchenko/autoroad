﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class AdvertCountViewRepository : BaseRepository<AdvertCountView>, IAdvertCountViewRepository
    {
        public AdvertCountViewRepository(HomeContext context)
            : base(context)
        {

        }
    }
}
