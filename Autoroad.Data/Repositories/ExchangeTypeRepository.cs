﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autoroad.Data.Repositories
{
	public class ExchangeTypeRepository : BaseRepository<ExchangeType>, IExchangeTypeRepository
	{
		public ExchangeTypeRepository(HomeContext context)
			: base(context)
		{
		}
	}
}