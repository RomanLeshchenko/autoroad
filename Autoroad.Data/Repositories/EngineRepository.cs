﻿using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;

namespace Autoroad.Data.Repositories
{
    public class EngineRepository : BaseRepository<Engine>, IEngineRepository
    {
        public EngineRepository(HomeContext context)
            :base(context)
        {

        }
    }
}
