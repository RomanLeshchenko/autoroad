﻿$(document).ready(function () {
    $("#ddlCars").change(function () {
        var idModel = $(this).val();
        $.getJSON("/PersonalCabinet/LoadModelsByBrand", { id: idModel },
                function (carData) {
                    var select = $("#ddlModels");
                    select.empty();
                    select.append($('<option/>', {
                        text: "Выберите модель"
                    }));
                    $.each(carData, function (index, itemData) {

                        select.append($('<option/>', {
                            value: itemData.Value,
                            text: itemData.Text
                        }));
                    });
                });
    });
});