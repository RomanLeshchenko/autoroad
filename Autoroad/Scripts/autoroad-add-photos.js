﻿$("#input-image-1").fileinput({
    uploadUrl: '/api/FileUpload/Upload',

    maxFileSize: 8000,
    maxFileCount: 12,
    validateInitialCount: true,
    uploadAsync: false,
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    browseClass: "btn btn-success",
    browseLabel: "Выберите фото",
    browseIcon: "<i class=\"glyphicon glyphicon-picture\"></i> ",
    removeClass: "btn btn-danger",
    removeLabel: "Удалить все фото",
    removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
    uploadClass: "btn btn-info",
    uploadLabel: "Загрузить фотографии и опубликовать объявление",
    uploadIcon: "<i class=\"glyphicon glyphicon-upload\"></i> ",

}).on('filebatchuploadsuccess', function (event, data, previewId, index) {
    var form = data.form, files = data.files, extra = data.extra,
        response = data.response, reader = data.reader;
    debugger;
    var input = $("#input-image-1");
    var advertID;
    try {
        advertID = input[0].attributes[4].nodeValue;
        var convertedAdvertID = parseInt(advertID);
        var isNumber = $.isNumeric(convertedAdvertID);

        if (isNumber) {
            window.location.href = '/PersonalCabinet/ConfirmPostAdvert?advertId=' + convertedAdvertID + '';
        } else {
            window.location.reload();
        }

    } catch (msg) {
        console.log("Error, cant find advert id");
    }
});



