﻿$(document).ready(function () {
    if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent))) {
        var succesSound = document.createElement("audio");
        succesSound.src = "/Sounds/success.wav";
        succesSound.volume = 0.10;
        succesSound.autoPlay = false;
        succesSound.preLoad = true;

        var errorSound = document.createElement("audio");
        errorSound.src = "/Sounds/error.wav";
        errorSound.volume = 0.10;
        errorSound.autoPlay = false;
        errorSound.preLoad = true;
    }

    $("#editUser").click(function () {
        model = new Object();
        model.ContactName = $("#conname").val();
        model.Phone = $("#phone").val();
        model.City = $("#city").val();
        model.VKContact = $("#vk").val();
        model.SkypeContact = $("#skype").val();
        model.FacebookContact = $("#fb").val();
        model.Login = $("#login").val();
        model.UserId = $("#userid").val();
        model.IsShowEmail = $("#showEmail:checked").val();
        model.IsShowCity = $("#showCity:checked").val();
        model.IsShowSkype = $("#showSkype:checked").val();
        model.IsShowVK = $("#showVk:checked").val();
        model.IsShowFacebook = $("#showFB:checked").val();
        model.IsNotifyAuction = $("#notifyAuction:checked").val();
        model.IsNotifyExchange = $("#notifyExchange:checked").val();

        $.ajax({
            url: "/api/User",
            type: "Post",
            data: model,
            success: function () {
                if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent))) {
                    succesSound.play();
                }
                
                notify({
                    type: "success", //alert | success | error | warning | info
                    title: "Успешно!",
                    position: {
                        x: "right", //right | left | center
                        y: "top" //top | bottom | center
                    },
                    icon: '<img src="/Images/paper_plane.png" />',
                    message: "Изменения были успешно сохранены.",
                    size: "small", //normal | full | small
                    overlay: false, //true | false
                    closeBtn: true, //true | false
                    overflowHide: false, //true | false
                    spacing: 20, //number px
                    theme: "default", //default | dark-theme
                    autoHide: true, //true | false
                    delay: 3500, //number ms
                    onShow: null, //function
                    onClick: null, //function
                    onHide: null, //function
                    template: '<div class="notify"><div class="notify-text"></div></div>'
                });
            },
            error: function () {
                errorSound.play();
                notify({
                    type: "error", //alert | success | error | warning | info
                    title: "Ошибка!",
                    theme: "dark-theme",
                    position: {
                        x: "right", //right | left | center
                        y: "top" //top | bottom | center
                    },
                    icon: '<img src="/Images/paper_plane.png" />',
                    message: "Изменения не были сохранены, пожалуйста, введите корректно Ваши данные.",
                    size: "small", //normal | full | small
                    overlay: false, //true | false
                    closeBtn: true, //true | false
                    overflowHide: true, //true | false
                    spacing: 20, //number px
                    theme: "default", //default | dark-theme
                    autoHide: true, //true | false
                    delay: 3500, //number ms
                    onShow: null, //function
                    onClick: null, //function
                    onHide: null, //function
                    template: '<div class="notify"><div class="notify-text"></div></div>'
                });
            }
        });
    });
});