﻿$(document).ready(function () {

    var exchange = $('#possible-exchange');

    if (exchange.is(':checked')) {
        $("#show-exchange").show();
    }

    $("#ddlCars").change(function () {
        var idModel = $(this).val();
        $.getJSON("/PersonalCabinet/LoadModelsByBrand", { id: idModel },
                function (carData) {
                    var select = $("#ddlModels");
                    select.empty();
                    select.append($('<option />', {
                    }));
                    $.each(carData, function (index, itemData) {

                        select.append($('<option />', {
                            value: itemData.Value,
                            text: itemData.Text
                        }));
                    });
                });
    });

    $("#ddlCarsExchange").change(function () {
        var idModel = $(this).val();
        $.getJSON("/PersonalCabinet/LoadModelsByBrand", { id: idModel },
                function (carData) {
                    var select = $("#ddlModelsExchange");
                    select.empty();
                    select.append($('<option />', {
                    }));
                    $.each(carData, function (index, itemData) {

                        select.append($('<option />', {
                            value: itemData.Value,
                            text: itemData.Text
                        }));
                    });
                });
    });

    $("#possible-exchange").change(function () {
        if (this.checked) {
            $("#show-exchange").fadeIn(400);
            $("#show-exchange").show();
        }
        else {
            $('.extype option:eq(0)').prop("selected", true);

            $('.brnd option:eq(0)').prop("selected", true);
            $('.mdl option:eq(0)').prop("selected", true);

            $('.bodyone option:eq(0)').prop("selected", true);
            $('.bodytwo option:eq(0)').prop("selected", true);

            $('.engineone option:eq(0)').prop("selected", true);
            $('.enginetwo option:eq(0)').prop("selected", true);

            $('.yearstart option:eq(0)').prop("selected", true);
            $('.yearend option:eq(0)').prop("selected", true);

            $('.kpp option:eq(0)').prop("selected", true);

            $('.pricestart').val('');
            $('.priceend').val('');
            $("#show-exchange").hide();
        }

    });
});