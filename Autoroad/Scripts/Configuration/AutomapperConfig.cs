﻿using AutoMapper;
using Autoroad.Core.ViewModels;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Autoroad.Configuration
{
    public static class AutomapperConfig
    {
        public static void Config(IMapperConfiguration cfg)
        {
            cfg.CreateMap<Advert, AdvertViewModel>();
        }
    }
}