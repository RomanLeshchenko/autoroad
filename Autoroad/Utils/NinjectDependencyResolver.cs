﻿using AutoMapper;
using Autoroad.Configuration;
using Autoroad.Core.IServices;
using Autoroad.Core.Services;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Repositories;
using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Autoroad.Utils
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            kernel.Bind<IMapper>().ToMethod(context =>
            {
                var config = new MapperConfiguration(AutomapperConfig.Config);
                var mapper = config.CreateMapper();
                return mapper;
            }).InSingletonScope();

            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IAdvertRepository>().To<AdvertRepository>();
            kernel.Bind<IBodyTypeRepository>().To<BodyTypeRepository>();
            kernel.Bind<IConditionRepository>().To<ConditionRepository>();
            kernel.Bind<IRegistrationRepository>().To<RegistrationRepository>();
            kernel.Bind<IRegionRepository>().To<RegionRepository>();
            kernel.Bind<ITransmissionRepository>().To<TransmissionRepository>();
            kernel.Bind<IFuelRepository>().To<FuelRepository>();
            kernel.Bind<IEngineRepository>().To<EngineRepository>();
            kernel.Bind<IReleaseYearRepository>().To<ReleaseYearRepository>();
            kernel.Bind<IBrandRepository>().To<BrandRepository>();
            kernel.Bind<IModelRepository>().To<ModelRepository>();
            kernel.Bind<IAdvertCountViewRepository>().To<AdvertCountViewRepository>();
            kernel.Bind<IFavouriteAdvertRepository>().To<FavouriteAdvertRepository>();
            kernel.Bind<IExchangeRepository>().To<ExchangeRepository>();
            kernel.Bind<IDriveUnitRepository>().To<DriveUnitRepository>();
            kernel.Bind<IPossibleExchangeCarRepository>().To<PossibleExchangeCarRepository>();
            kernel.Bind<IExchangeTypeRepository>().To<ExchangeTypeRepository>();
            kernel.Bind<IAdvertPhotoRepository>().To<AdvertPhotoRepository>();
            kernel.Bind<IPhotoInAdvertRepository>().To<PhotoInAdvertRepository>();

            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IPhotoService>().To<PhotoService>();
            kernel.Bind<IAdvertService>().To<AdvertService>();
            kernel.Bind<IBrandService>().To<BrandService>();
            kernel.Bind<IModelService>().To<ModelService>();
            kernel.Bind<IReleaseYearService>().To<ReleaseYearService>();
            kernel.Bind<IEngineService>().To<EngineService>();
            kernel.Bind<IFuelService>().To<FuelService>();
            kernel.Bind<ITransmissionService>().To<TransmissionService>();
            kernel.Bind<IBodyTypeService>().To<BodyTypeService>();
            kernel.Bind<IRegionService>().To<RegionService>();
            kernel.Bind<IRegistrationService>().To<RegistrationService>();
            kernel.Bind<IConditionService>().To<ConditionService>();
            kernel.Bind<IExchangeService>().To<ExchangeService>();
            kernel.Bind<IDriveUnitService>().To<DriveUnitService>();
        }
    }
}