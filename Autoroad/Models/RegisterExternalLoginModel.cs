﻿using System.ComponentModel.DataAnnotations;


namespace Autoroad.Models
{
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "Логин")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }
}
