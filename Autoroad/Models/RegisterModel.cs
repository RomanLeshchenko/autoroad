﻿using System.ComponentModel.DataAnnotations;

namespace Autoroad.Models
{
	public class RegisterModel
	{
		[Required(ErrorMessage = "Введите E-mail")]
		[EmailAddress(ErrorMessage = "Некорректный E-mail")]
		[Display(Name = "E-mail")]
		public string UserName { get; set; }

		[Required(ErrorMessage = "Введите пароль")]
		[StringLength(100, ErrorMessage = "{0} должен быть не менее {2} символов", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Пароль")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Подтвердите пароль")]
		[Compare("Password", ErrorMessage = "Пароль и подтверждение пароля не совпадают")]
		public string ConfirmPassword { get; set; }

		[Required(ErrorMessage = "Введите имя")]
		[Display(Name = "Контактное имя")]
		[RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Контактное имя может содержать только буквы!")]
		[StringLength(20, MinimumLength = 2, ErrorMessage = "Контактное имя должно иметь хотя бы 2 буквы и максимум 20")]
		public string ContactName { get; set; }

		[Required(ErrorMessage = "Введите телефон")]
		[Display(Name = "Телефон")]
		[RegularExpression(@"^[0-9]+$", ErrorMessage = "Телефон не может включать в себя другие символы кроме цифр")]
		[StringLength(11, MinimumLength = 11, ErrorMessage = "Введите номер в таком формате: 80971234567 ")]
		public string Phone { get; set; }

		[Required(ErrorMessage = "Введите город")]
		[Display(Name = "Город")]
		[RegularExpression(@"^[а-яА-ЯёЁa-zA-Z]+$", ErrorMessage = "Город может содержать только буквы")]
		[StringLength(20, MinimumLength = 2, ErrorMessage = "Город должен иметь хотя бы 2 буквы и максимум 20")]
		public string City { get; set; }

		[Display(Name = "Skype")]
		public string SkypeContact { get; set; }

		[Display(Name = "Вконтакте")]
		public string VKContact { get; set; }

		[Display(Name = "Facebook")]
		public string FacebookContact { get; set; }
	}
}