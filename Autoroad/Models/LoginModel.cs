﻿using System.ComponentModel.DataAnnotations;

namespace Autoroad.Models
{
	public class LoginModel
	{
		[Required]
		[EmailAddress(ErrorMessage = "Введите корректный E-mail")]
		[Display(Name = "E-mail")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Пароль")]
		public string Password { get; set; }

		[Display(Name = "Запомнить меня?")]
		public bool RememberMe { get; set; }
	}
}