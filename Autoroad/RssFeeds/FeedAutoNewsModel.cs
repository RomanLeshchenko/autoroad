﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Autoroad.RssFeeds
{
    public class FeedAutoNewsModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string PublicDate { get; set; }
        public string Image { get; set; }
    }
}