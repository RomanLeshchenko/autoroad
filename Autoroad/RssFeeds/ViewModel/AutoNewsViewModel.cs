﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Autoroad.RssFeeds.ViewModel
{
    public class AutoNewsViewModel
    {
        public List<FeedAutoNewsModel> ListAutoNews { get; set; }
    }
}