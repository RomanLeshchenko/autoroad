﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Autoroad.RssFeeds
{
    public class News
    {
        public static List<FeedAutoNewsModel> GetFeed()
        {
            var client = new WebClient();

            XmlDocument doc = new XmlDocument();

            doc.Load(@ConfigurationManager.AppSettings["XmlInfoData"]);
            XmlNode versionNode = doc.SelectSingleNode("//rss");
            string value = versionNode.InnerXml;

            XDocument xml = XDocument.Parse(value);

            try
            {
                var AutoNewsUpdates = (from story in xml.Descendants("item")
                                       select new FeedAutoNewsModel
                                       {
                                           Title = ((string)story.Element(ConfigurationManager.AppSettings["RssTitle"])),
                                           Link = ((string)story.Element(ConfigurationManager.AppSettings["RssLink"])),
                                           Description = ((string)story.Element(ConfigurationManager.AppSettings["RssDescription"])),
                                           PublicDate = ((string)story.Element(ConfigurationManager.AppSettings["RssPubDate"])),
                                           Image = story.Descendants(ConfigurationManager.AppSettings["RssImage"]).First().ToString()

                                       }).Take(4).ToList();
                return AutoNewsUpdates;
            }
            catch
            {
                throw new FormatException();
            }
        }
    }
}