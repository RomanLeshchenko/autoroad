﻿using Autoroad.Core.IServices;
using Autoroad.Data.DbContexts;
using Autoroad.Data.IRepositories;
using Autoroad.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace Autoroad.Controllers
{
    [Authorize]
    public class PersonalCabinetController : Controller
    {
        private readonly IUserService _userService;
        private readonly IAdvertService _advertService;
        private readonly IConditionService _conditionService;
        private readonly IRegistrationService _registrationService;
        private readonly IRegionService _regionService;
        private readonly IBodyTypeService _bodyTypeService;
        private readonly ITransmissionService _transmissionService;
        private readonly IFuelService _fuelService;
        private readonly IEngineService _engineService;
        private readonly IReleaseYearService _releaseYearService;
        private readonly IBrandService _brandService;
        private readonly IModelService _modelService;
        private readonly IDriveUnitService _driveUnitService;
        private readonly IAdvertRepository _advertRepository;
        private readonly IExchangeTypeRepository _exchangeTypeRepository;

        public PersonalCabinetController(IExchangeTypeRepository exchangeTypeRepository, IAdvertRepository advertRepository, IDriveUnitService driveUnitService, IAdvertService advertService, IConditionService conditionService, IRegistrationService registrationService, IRegionService regionService, IBodyTypeService bodyTypeService, ITransmissionService transmissionService, IFuelService fuelService, IEngineService engineService, IReleaseYearService releaseYearService, IBrandService brandService, IModelService modelService, IUserService userService)
        {
            _advertService = advertService;
            _userService = userService;
            _conditionService = conditionService;
            _registrationService = registrationService;
            _regionService = regionService;
            _bodyTypeService = bodyTypeService;
            _transmissionService = transmissionService;
            _fuelService = fuelService;
            _engineService = engineService;
            _releaseYearService = releaseYearService;
            _brandService = brandService;
            _modelService = modelService;
            _driveUnitService = driveUnitService;
            _advertRepository = advertRepository;
            _exchangeTypeRepository = exchangeTypeRepository;
        }

        [HttpGet]
        public ActionResult PersonalData()
        {
            var userId = WebSecurity.CurrentUserId;

            var user = _userService.GetUserPersonalData(userId);
            return View(user);
        }

        public ActionResult UpdateAdvert(int advertId)
        {
            var user = WebSecurity.CurrentUserId;
            _advertService.UpdateAdvertDateAdded(user, advertId);
            return View();
        }

        [HttpGet]
        public ActionResult PersonalAdverts()
        {
            var userId = WebSecurity.CurrentUserId;
            var adverts = _advertService.GetUserAdverts(userId);

            return View(adverts);
        }

        [HttpGet]
        public ActionResult AddAdvert()
        {
            InitializeViewBag();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAdvert(Advert model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = WebSecurity.GetUserId(User.Identity.Name);
                var advert = _advertService.AddAdvert(model.UserId, model);
                return RedirectToAction("AddPhotosToAdvert", "PersonalCabinet", new { advertId = advert.AdvertId });
            }

            InitializeViewBag();
            return View();
        }

        [HttpGet]
        public ActionResult AddPhotosToAdvert(int advertId)
        {
            var user = WebSecurity.CurrentUserId;

            var advert = _advertRepository.Get(x => x.AdvertId == advertId &&
                                                    x.UserId == user && (x.AdvertStatus == AdvertStatus.AddingPhotosProccess || x.AdvertStatus == AdvertStatus.ConfirmationAddingProccess));
            if (advert != null)
            {
                ViewBag.AdvertId = advert.AdvertId;
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult RemoveAdvert(int advertId)
        {
            var userId = WebSecurity.CurrentUserId;
            _advertService.RemoveAdvert(userId, advertId);
            return RedirectToAction("PersonalAdverts");
        }

        [HttpGet]
        public ActionResult ConfirmPostAdvert(int advertId)
        {
            var user = WebSecurity.CurrentUserId;

            var advert = _advertRepository.Get(x => x.AdvertId == advertId && x.isPublished == false &&
                                                    x.UserId == user && (x.AdvertStatus == AdvertStatus.AddingPhotosProccess || x.AdvertStatus == AdvertStatus.ConfirmationAddingProccess));

            if (advert != null)
            {
                ViewBag.AdvertId = advert.AdvertId;
                return View();
            }

            return RedirectToAction("Index", "Home");
        }


        public ActionResult ConfirmationPostAdvert(int advertId)
        {
            var user = WebSecurity.CurrentUserId;

            var advert = _advertRepository.Get(x => x.AdvertId == advertId && x.isPublished == false &&
                                                    x.UserId == user && (x.AdvertStatus == AdvertStatus.AddingPhotosProccess || x.AdvertStatus == AdvertStatus.ConfirmationAddingProccess));
            if (advert != null)
            {
                advert.isPublished = true;
                advert.DateAdded = DateTime.Now;
                advert.DateUpdated = DateTime.Now;

                _advertRepository.Save();
            }

            return RedirectToAction("PersonalAdverts");
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadModelsByBrand(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var modelList = _modelService.GetModels(Convert.ToInt32(id));
                var modelData = modelList.Select(m => new SelectListItem()
                {
                    Text = m.Name,
                    Value = m.ModelId.ToString(),
                });

                return Json(modelData, JsonRequestBehavior.AllowGet);
            }

            List<SelectListItem> emptyList = new List<SelectListItem>();
            return Json(emptyList, JsonRequestBehavior.AllowGet);
        }

        private void InitializeViewBag()
        {
            ViewBag.BodyTypes = new SelectList(_bodyTypeService.GetBodyTypes(), "BodyTypeId", "Name");
            ViewBag.ReleaseYear = new SelectList(_releaseYearService.GetReleaseYears().OrderByDescending(x => x.Year), "ReleaseYearId", "Year");
            ViewBag.Engine = new SelectList(_engineService.GetEngines(), "EngineId", "Name");
            ViewBag.Fuel = new SelectList(_fuelService.GetFuels(), "FuelId", "Name");
            ViewBag.Transmission = new SelectList(_transmissionService.GetTransmissions(), "TransmissionId", "Name");
            ViewBag.Region = new SelectList(_regionService.GetRegions(), "RegionId", "Name");
            ViewBag.Condition = new SelectList(_conditionService.GetConditions(), "ConditionId", "Name");
            ViewBag.Registration = new SelectList(_registrationService.GetRegistrations(), "RegistrationId", "Name");
            ViewBag.DriveUnit = new SelectList(_driveUnitService.GetDriveUnits(), "DriveUnitId", "Name");
            ViewBag.Brand = _brandService.GetBrands().OrderBy(a => a.Name).ToList();
            ViewBag.ModelAutos = _modelService.GetModels().OrderBy(a => a.Name).ToList();
            ViewBag.ExchangeTypes = new SelectList(_exchangeTypeRepository.GetAll(), "ExchangeTypeId", "TypeName");
        }

    }
}