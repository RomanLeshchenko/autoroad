﻿using Autoroad.Core.IServices;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebMatrix.WebData;

namespace Autoroad.Controllers.APIControllers
{
    public class FileUploadController : ApiController
    {
        private readonly IPhotoService _photoService;

        public FileUploadController(IPhotoService photoService)
        {
            _photoService = photoService;
        }


        [HttpGet]
        public IHttpActionResult GetExistingFiles()
        {
            var result = _photoService.GetExistingPhotos();

            return Json(new { Data = result });
        }

        // TODO : проверить на если юзер не тот или нету айди
        [HttpPost]
        public IHttpActionResult Upload(int advertId)
        {
            var currentUserId = WebSecurity.CurrentUserId;

            var context = HttpContext.Current;
            if (context.Request.Files.Count > 0 && context.Request.Files.Count <= 12)
            {
                HttpFileCollection files = HttpContext.Current.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {
                        HttpPostedFile file = files[i];

                        var newFileName = _photoService.GetNewFileName(file);
                        if (string.IsNullOrEmpty(newFileName))
                        {
                            continue;
                        }

                        string fname = context.Server.MapPath("~/Uploads/" + newFileName);
                        Stream strm = file.InputStream;
                        var targetFile = fname;

                        if (file.ContentType != "image/png")
                        {
                            _photoService.GenerateThumbnails(0.6, strm, targetFile, newFileName, advertId);
                        }
                        else
                        {
                            _photoService.SavePNGFile(file, fname, newFileName, advertId);
                        }
                    }
                    catch (Exception ex)
                    {
                        var exception = ex;
                        continue;
                    }
                }
                context.Response.ContentType = "application/json";
                context.Response.Write("{}");

                return Ok();
            }

            return BadRequest();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
