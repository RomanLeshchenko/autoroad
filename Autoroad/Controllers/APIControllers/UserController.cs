﻿using Autoroad.Core.IServices;
using Autoroad.Data.Models;
using System.Web.Http;

namespace Autoroad.Controllers.APIControllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Edit user personal data.
        /// </summary>
        /// <param name="model">User model</param>
        /// <returns></returns>
        public IHttpActionResult Post(User model)
        {
            if (ModelState.IsValid)
            {
                var result = _userService.EditUserPersonalData(model);

                if (result != null)
                {
                    return Ok();
                }
            }

            return BadRequest();
        }
    }
}
