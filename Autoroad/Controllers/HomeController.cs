﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Autoroad.Models;
using WebMatrix.WebData;
using Autoroad.RssFeeds.ViewModel;
using Autoroad.RssFeeds;
using Autoroad.Data.Models;
using Autoroad.Data.DbContexts;
using Autoroad.Core.IServices;
using Autoroad.Data.ViewModels;

namespace Autoroad.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAdvertService _advertService;
        private readonly IConditionService _conditionService;
        private readonly IRegistrationService _registrationService;
        private readonly IRegionService _regionService;
        private readonly IBodyTypeService _bodyTypeService;
        private readonly ITransmissionService _transmissionService;
        private readonly IFuelService _fuelService;
        private readonly IEngineService _engineService;
        private readonly IReleaseYearService _releaseYearService;
        private readonly IBrandService _brandService;
        private readonly IModelService _modelService;

        public HomeController(IAdvertService advertService, IConditionService conditionService, IRegistrationService registrationService, IRegionService regionService, IBodyTypeService bodyTypeService, ITransmissionService transmissionService, IFuelService fuelService, IEngineService engineService, IReleaseYearService releaseYearService, IBrandService brandService, IModelService modelService)
        {
            _advertService = advertService;
            _conditionService = conditionService;
            _registrationService = registrationService;
            _regionService = regionService;
            _bodyTypeService = bodyTypeService;
            _transmissionService = transmissionService;
            _fuelService = fuelService;
            _engineService = engineService;
            _releaseYearService = releaseYearService;
            _brandService = brandService;
            _modelService = modelService;
        }

        /// <summary>
        /// Home page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index()
        {
            var search = _advertService.SearchAdvert();

            ViewBag.BodyTypes = new SelectList(_bodyTypeService.GetBodyTypes(), "BodyTypeId", "Name");
            ViewBag.ReleaseYear = new SelectList(_releaseYearService.GetReleaseYears().OrderByDescending(x=>x.Year), "ReleaseYearId", "Year");
            ViewBag.Engine = new SelectList(_engineService.GetEngines(), "EngineId", "Name");
            ViewBag.Fuel = new SelectList(_fuelService.GetFuels(), "FuelId", "Name");
            ViewBag.Transmission = new SelectList(_transmissionService.GetTransmissions(), "TransmissionId", "Name");
            ViewBag.Region = new SelectList(_regionService.GetRegions(), "RegionId", "Name");
            ViewBag.Condition = new SelectList(_conditionService.GetConditions(), "ConditionId", "Name");
            ViewBag.Registration = new SelectList(_registrationService.GetRegistrations(), "RegistrationId", "Name");
            ViewBag.Brand = _brandService.GetBrands().OrderBy(a => a.Name).ToList();
            ViewBag.ModelAutos = _modelService.GetModels().OrderBy(a => a.Name).ToList();

            // AutoNewsViewModel model = new AutoNewsViewModel();
            //model.ListAutoNews = News.GetFeed();
            return View(search);

        }

        public ActionResult Test(SearchAdvertViewModel model)
        {
            _advertService.GetSearchAdvertResult(model);
            return View();
        }
    }
}
